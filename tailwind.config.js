module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        bluemain: "#DCE9F9",
        skyblue: "#4094F7",
        greensecondary: "#1AC19D",
        lightgray: "#6E8BB7",
        purple: "#A23FEE",
      },
      screens: {
        "2xl": { max: "1535px" },
        // => @media (max-width: 1535px) { ... }

        xl: { max: "1279px" },
        // => @media (max-width: 1279px) { ... }

        lg: { max: "1023px" },
        // => @media (max-width: 1023px) { ... }

        md: { max: "767px" },
        // => @media (max-width: 767px) { ... }

        sm: { max: "639px" },
        // => @media (max-width: 639px) { ... }
        xsm: { max: "375" },
        // => @media (max-width: 375) { ... }
      },
      width: {
        600: "600px",
      },
    },
  },
  plugins: [],
};
