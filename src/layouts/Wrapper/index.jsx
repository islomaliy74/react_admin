import AnalyticsCom from "../../components/analytics/AnalyticsCom";
import Navbar from "@layouts/Navbar";
export default function App({ children }) {
  return (
    <div>
      <Navbar />
      <div className="bg-bluemain ml-20  -mt-4 ">
        <AnalyticsCom />
      </div>
    </div>
  );
}
