// Containers
import AuthContainer from "@containers/Auth_container/index";
import MainContainer from "@containers/Main_container/index";
import EmptyContiner from "@containers/Empty_container/index";
// pages
// import P404 from "@pages/exceptions/PageNotFound";
import Dashboard from "@pages/dashboard/index";
import Login from "@pages/login";

import P404 from "../components/exceptions/P404";
import P403 from "../components/exceptions/P403";
import P502 from "../components/exceptions/P502";
export const routes = [
  {
    path: "/*",
    title: "dashboard",
    hideChildren: false,
    component: MainContainer,
    children: [
      {
        path: "main",
        title: "Dashboard",
        component: Dashboard,
        children: [],
      },
    ],
  },
  {
    path: "/*",
    title: "login",
    hideChildren: false,
    component: AuthContainer,
    children: [
      {
        path: "auth",
        title: "auth",
        component: Login,
        children: [],
      },
    ],
  },
  {
    path: "/exception",
    title: "exception",
    hideChildren: false,
    component: EmptyContiner,
    children: [
      {
        path: "404",
        title: "Not found",
        component: P404,
        children: [],
      },
    ],
  },
  {
    path: "/404",
    title: "404 Error",
    hideChildren: false,
    component: P404,
    children: [],
  },
  {
    path: "/403",
    title: "403 Error",
    hideChildren: false,
    component: P403,
    children: [],
  },
  {
    path: "/502",
    title: "502 Error",
    hideChildren: false,
    component: P502,
    children: [],
  },
];
