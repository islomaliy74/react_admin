import React from "react";
import StoreIcon from "../../assets/icons/store2.svg";
import UsersIcon from "../../assets/icons/super2.svg";
import NotIcon from "../../assets/icons/notification2.svg";
import CarIcon from "../../assets/icons/car.svg";
import Red from "../../assets/icons/red.png";
import Yellow from "../../assets/icons/yellow.png";
import Purple from "../../assets/icons/purple.png";
import { KeyboardArrowUp } from "@mui/icons-material";
import Orders from "./Orders";
import Overall from "./Overall";
export default function AnalyticsCom() {
  return (
    <div className="mt-4 ml-28 w-3/4 2xl:1/2 sm:m-4 sm:w-full">
      <div className="flex sm:flex-wrap ">
        <ShortAnalytics title="24" description="Филиалы" imgIcon={StoreIcon} />
        <ShortAnalytics
          title="110,823"
          description="Клиенты"
          imgIcon={UsersIcon}
        />
        <ShortAnalytics
          title="110,823"
          description="Подписчики"
          imgIcon={NotIcon}
        />
        <ShortAnalytics title="85" description="Курьеры" imgIcon={CarIcon} />
      </div>
      <div className="flex sm:flex-wrap">
        <div className="flex flex-col ">
          <Earnings
            title="Total orders this month"
            earings="1,850"
            percent="+5%"
            position={"up"}
            img={Yellow}
          />
          <Earnings
            title="Total orders this month"
            earings="16,250"
            percent="+25%"
            position={"up"}
            img={Purple}
          />
          <Earnings
            title="Total orders this month"
            earings="12,750"
            percent="+12%"
            position={"up"}
            img={Red}
          />
        </div>
        <Orders />
      </div>
      <Overall />
    </div>
  );
}

function ShortAnalytics(props) {
  return (
    <div className="sm:w-full bg-white mr-6 mt-4 flex justify-around pt-4 pl-2 pr-2 rounded-lg border">
      <div className="ml-4 ">
        <h1 className="text-blue-500 font-semibold text-3xl">{props.title}</h1>
        <p className="text-gray-500 pt-5 mb-4">{props.description} </p>
      </div>
      <div className="ml-16 bg-blue-200 p-8 rounded-lg mb-4 mr-2 items-center ">
        <img className="w-full h-full" alt="icon" src={props.imgIcon} />
      </div>
    </div>
  );
}

function Earnings(props) {
  return (
    <div className="sm:w-fit bg-white mt-5 pl-5 rounded-lg border ">
      <div className="mt-4 text-gray-500">{props.title}</div>
      <div className=" mt-4 flex justify-between items-baseline mb-4">
        <h1 className=" text-3xl font-semibold">${props.earings}</h1>
        <div className=" flex justify-between items-center ml-4 mr-6 pl-4 rounded-2xl text-green-500 mb-2  bg-green-500/20 backdrop-blur-sm ">
          <h1 className="font-semibold mr-4 ">{props.percent}</h1>
          <div className="rounded-full bg-green-500/40 mr-2 mb-2 mt-2">
            <KeyboardArrowUp />
          </div>
        </div>
      </div>
      <img className="mb-4 mr-6" src={props.img} alt="diagram-img" />
    </div>
  );
}
