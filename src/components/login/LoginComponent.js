import React from "react";
import LoginImg from "../../assets/images/login-img.png";
import UserIcon from "../../assets/icons/user.svg";
import PassIcon from "../../assets/icons/pass.svg";
export default function LoginComponent() {
  return (
    <div>
      <div className="flex justify-center flex-wrap-reverse">
        <div className=" flex w-1/2  flex-col justify-center align-middle h-screen bg-login-page">
          <h1 className="absolute top-10 left-10 text-white text-5xl font-bold">
            Bootcamp
          </h1>
          <div className="flex justify-center mr-10">
            <img src={LoginImg} alt="login-bg-img" />
          </div>
        </div>
        <div className="  w-1/2 e h-screen ">
          <div className="w-4/5 h-screen m-auto flex flex-col  justify-center align-middle text-left relative">
            <h1 className="mb-10 text-4xl font-bold text-blue-dark">
              Вход в платформу
            </h1>
            <div className="flex flex-col justify-center mb-48">
              <label className="mb-2 font-semibold">
                Имя пользователя <span className="text-red-600">*</span>
              </label>
              <div className="relative w-full">
                <input
                  className="w-full mb-8 px-16 py-4 rounded-md border border-gray-light focus-visible:border-blue-light"
                  placeholder="Ввдите имя"
                />
                <img
                  className="absolute top-5 left-4 opacity-80"
                  src={UserIcon}
                  alt="user-icon"
                />
              </div>
              <label className="mb-2 font-semibold">
                Пароль <span className="text-red-600">*</span>
              </label>
              <div className="relative w-full">
                <input
                  className="w-full mb-8 px-16 py-4 rounded-md border border-gray-light focus-visible:border-blue-light"
                  placeholder="Ввдите пароль"
                />
                <img
                  className="absolute top-5 left-4 "
                  src={PassIcon}
                  alt="pass-icon"
                />
              </div>
              <div className="flex items-center mb-5">
                <input type="checkbox" />
                <p className="ml-4 opacity-30">Запомнить меня</p>
              </div>
              <button className="rounded-xl py-4 bg-blue-600 text-white">
                Войти
              </button>
            </div>

            <p className="absolute bottom-10 left-0 opacity-30">
              Copyright ©Udevs. Все права защищены
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
