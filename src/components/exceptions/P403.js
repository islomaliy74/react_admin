import React from "react";
import Pic403 from "../../assets/images/403.png";
export default function P403() {
  return (
    <div className="flex mt-60 justify-center  flex-wrap">
      <div>
        <h1 className=" text-blue-500  text-9xl font-semibold ">403</h1>
        <h2 className="font-semibold text-4xl mt-10 ">Access Forbidden</h2>
        <p className="mt-10">Sorry, the page you visited does not exist.</p>
        <button className="bg-blue-500 p-2 text-white rounded mt-14">
          Вернутся в главную
        </button>
      </div>
      <div className="ml-20">
        <img src={Pic403} alt="403-img" />
      </div>
    </div>
  );
}
