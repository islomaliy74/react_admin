import React from "react";
import Pic404 from "../../assets/images/404.png";
export default function P404() {
  return (
    <div className="flex mt-60 justify-center  flex-wrap">
      <div>
        <h1 className=" text-blue-500  text-9xl font-semibold ">404</h1>
        <h2 className="font-semibold text-4xl mt-10 ">Page doesn't exist</h2>
        <p className="mt-10">Sorry, the page you visited does not exist.</p>
        <button className="bg-blue-500 p-2 text-white rounded mt-14">
          Вернутся в главную
        </button>
      </div>
      <div className="ml-20">
        <img src={Pic404} alt="404-img" />
      </div>
    </div>
  );
}
