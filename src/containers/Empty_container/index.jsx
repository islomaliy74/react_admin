import { Outlet } from "react-router-dom";

export default function EmptyContainer() {
  return (
    <div>
      <P403 />
      <P404 />
      <P502 />
      <div>{<Outlet />}</div>
    </div>
  );
}

function P403() {
  return <div>403</div>;
}

function P404() {
  return <div>404</div>;
}

function P502() {
  <div>502</div>;
}
