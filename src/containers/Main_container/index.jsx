// import { Outlet } from "react-router-dom";

// import Navbar from "@layouts/Navbar";
import Sidebar from "@layouts/Sidebar";
import Wrapper from "@layouts/Wrapper";

export default function MainContainer() {
  return (
    <div>
      <Sidebar />
      <Wrapper />
    </div>
  );
}
